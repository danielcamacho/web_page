const express = require('express');
const router = express.Router();

// Se muestra la plantilla index.pug
router.get('/', (req, res) => {
    res.render('index', { tittle: 'Inicio' });
});

//vista admin.pug
router.get('/admin', (req, res) => {
    res.render('admin', { tittle: 'Administrador' });
});

//formulario Singup
router.get('/singup', (req, res) => {
    res.render('singup', { tittle: 'Registro' })
})

module.exports = router;