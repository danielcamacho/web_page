const express = require('express');
const app = express();

//dependencias
const path = require('path');
const mongoose = require('mongoose');

//configuracion
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

//conectar mongoose
const { url } = require('./routes/routes_db');
mongoose.connect(url, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    })
    .then(() => console.log('DB Connected!'))
    .catch(err => {
        console.log(`db error ${err}`);
    });

//routes view
app.use(require('./routes/routes_views'))

//archivo estatico --> static files
app.use(express.static(path.join(__dirname, 'public')));

//servidor
app.listen(app.get('port'), () => {
    console.log("Server on port", app.get('port'))
})